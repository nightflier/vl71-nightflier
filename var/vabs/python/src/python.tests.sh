#!/usr/bin/bash

# Regression test: test we can import the threading modules
echo "We can import threading and related modules"
echo

echo "Importing threading"
python -c "import threading" || exit 1
echo "ok"

echo "Importing thread"
python -c "import thread" || exit 2
echo "ok"

echo "Importing subprocess"
python -c "import subprocess" || exit 3
echo "ok"

echo "Importing Queue"
python -c "import Queue" || exit 4
echo "ok"

echo
echo "Threading tests ok"
