#!/bin/sh
# GCC package build script (written by volkerdi@slackware.com)
#
# Copyright 2003, 2004  Slackware Linux, Inc., Concord, California, USA
# Copyright 2005, 2006, 2007, 2008, 2009  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Some notes, Fri May 16 12:31:32 PDT 2003:
#
# Why i486 and not i386?  Because the shared C++ libraries in gcc-3.2.x will
# require 486 opcodes even when a 386 target is used (so we already weren't
# compatible with the i386 for Slackware 9.0, didn't notice, and nobody
# complained :-).  gcc-3.3 fixes this issue and allows you to build a 386
# compiler, but the fix is done in a way that produces binaries that are not
# compatible with gcc-3.2.x compiled binaries.  To retain compatibility with
# Slackware 9.0, we'll have to use i486 (or better) as the compiler target
# for gcc-3.3.
#
# It's time to say goodbye to i386 support in Slackware.  I've surveyed 386
# usage online, and the most common thing I see people say when someone asks
# about running Linux on a 386 is to "run Slackware", but then they also 
# usually go on to say "be sure to get an OLD version, like 4.0, before glibc,
# because it'll be more efficient."  Now, if that's the general advice, then
# I see no reason to continue 386 support in the latest Slackware (and indeed
# it's no longer easily possible).

PKGNAM=gcc
VERSION="4.7.2" #${VERSION:-$(echo $PKGNAM-*.tar.xz | rev | cut -f 3- -d . | cut -f 1 -d - | rev)}
#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"3"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK="http://ftp.gnu.org/gnu/$PKGNAM/$PKGNAM-$VERSION/$PKGNAM-$VERSION.tar.bz2 http://slackware.cs.utah.edu/pub/slackware/slackware-current/source/d/gcc/fastjar-0.97.tar.xz"
MAKEDEPENDS=${MAKEDEPENDS:-"kernel-headers glibc gcc gcc-g++ gcc-java jre infozip"} #Add deps needed TO BUILD this package here.

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$(uname -m)" in
    i?86) ARCH=i586 ;;
    arm*) readelf /usr/bin/file -A | egrep -q "Tag_CPU.*[4,5]" && ARCH=arm || ARCH=armv7lh ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *) ARCH=$(uname -m) ;;
  esac
  export $ARCH
fi

# How many jobs to run in parallel:
NUMJOB=" -j 4 "

if [ "$ARCH" = "i386" ]; then
  SLKCFLAGS="-O2 -march=i386 -mcpu=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
fi

case "$ARCH" in
    arm*) TARGET=$ARCH-vlocity-linux-gnueabi ;;
    i?86)    TARGET=$ARCH-vector-linux ;;
    *)    TARGET=$ARCH-vlocity-linux ;;
esac

#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------

CWD=$(pwd)
cd ../
PKGDIR=$(pwd)
cd $CWD
# Temporary build location.  This should *NOT* be a directory
# path a non-root user could create later...
TMP=/gcc-$(mcookie)

# This is the main DESTDIR target:
PKG1=$TMP/package-gcc
# These are the directories to build other packages in:
PKG2=$TMP/package-gcc-g++
PKG3=$TMP/package-gcc-gfortran
PKG4=$TMP/package-gcc-gnat
PKG5=$TMP/package-gcc-java
PKG6=$TMP/package-gcc-objc
PKG7=$TMP/package-gcc-g++-gch

# Clear the build locations:
if [ -d $TMP ]; then
  rm -rf $TMP
fi
mkdir -p $PKG{1,2,3,4,5,6,7}/usr/doc/gcc-$VERSION

# Insert package descriptions:
mkdir -p $PKG{1,2,3,4,5,6,7}/install
cat $CWD/slack-desc.gcc > $PKG1/install/slack-desc
cat $CWD/slack-desc.gcc-g++ > $PKG2/install/slack-desc
cat $CWD/slack-desc.gcc-gfortran > $PKG3/install/slack-desc
cat $CWD/slack-desc.gcc-gnat > $PKG4/install/slack-desc
cat $CWD/slack-desc.gcc-java > $PKG5/install/slack-desc
cat $CWD/slack-desc.gcc-objc > $PKG6/install/slack-desc
# cat $CWD/slack-desc.gcc-g++-gch > $PKG7/install/slack-desc

cd $TMP
tar xvf $CWD/gcc-$VERSION.tar.?z*

# Copy ecj.jar into the TLD of the source. Needed for java compiler.
# This can be retrieved from ftp://sourceware.org/pub/java
cp $CWD/ecj-4.3.jar gcc-$VERSION/ecj.jar

# install docs
( cd gcc-$VERSION
  # Smite the fixincludes:
  zcat $CWD/gcc-no_fixincludes.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1
  # Fix perms/owners
  chown -R root:root .
  find . -perm 777 -exec chmod 755 {} \;
  find . -perm 775 -exec chmod 755 {} \;
  find . -perm 754 -exec chmod 755 {} \;
  find . -perm 664 -exec chmod 644 {} \;
  mkdir -p $PKG1/usr/doc/gcc-$VERSION
  # Only the most recent ChangeLog... shouldn't be too big. :)
  cp -a \
    BUGS COPYING* ChangeLog \
    ChangeLog.tree-ssa FAQ INSTALL \
    LAST_UPDATED MAINTAINERS NEWS \
    README* *.html \
  $PKG1/usr/doc/gcc-$VERSION

  mkdir -p $PKG1/usr/doc/gcc-${VERSION}/gcc
  ( cd gcc
    cp -a \
      ABOUT* COPYING* LANG* ONEWS README* SERVICE \
    $PKG1/usr/doc/gcc-$VERSION/gcc
         
    mkdir -p $PKG3/usr/doc/gcc-${VERSION}/gcc/fortran
    ( cd fortran
      cp -a \
        ChangeLog \
      $PKG3/usr/doc/gcc-$VERSION/gcc/fortran/ChangeLog
    )

    mkdir -p $PKG4/usr/doc/gcc-${VERSION}/gcc/ada
    ( cd ada
      cp -a \
        ChangeLog ChangeLog.tree-ssa \
      $PKG4/usr/doc/gcc-$VERSION/gcc/ada
    )

    mkdir -p $PKG5/usr/doc/gcc-${VERSION}/gcc/java
    ( cd java
      cp -a \
        ChangeLog ChangeLog.tree-ssa \
      $PKG5/usr/doc/gcc-${VERSION}/gcc/java
    )
  
    mkdir -p $PKG6/usr/doc/gcc-${VERSION}/gcc/objc
    ( cd objc
      cp -a \
        ChangeLog README \
      $PKG6/usr/doc/gcc-${VERSION}/gcc/objc
    )
  ) || exit 1

  mkdir -p $PKG3/usr/doc/gcc-${VERSION}/libgfortran
  ( cd libgfortran
    cp -a \
      ChangeLog \
    $PKG3/usr/doc/gcc-${VERSION}/libgfortran/ChangeLog
  )

  mkdir -p $PKG3/usr/doc/gcc-${VERSION}/libada
  ( cd libada
    cp -a \
      ChangeLog \
    $PKG3/usr/doc/gcc-${VERSION}/libada
  )

  mkdir -p $PKG5/usr/doc/gcc-${VERSION}/libffi
  ( cd libffi
    cp -a \
      ChangeLog ChangeLog.libgcj ChangeLog.v1 \
      LICENSE README \
    $PKG5/usr/doc/gcc-${VERSION}/libffi
  )

  mkdir -p $PKG5/usr/doc/gcc-${VERSION}/libjava
  ( cd libjava
    cp -a \
      COPYING* ChangeLog HACKING LIBGCJ_LICENSE \
      NEWS README THANKS \
    $PKG5/usr/doc/gcc-${VERSION}/libjava
  )

  mkdir -p $PKG1/usr/doc/gcc-${VERSION}/libmudflap
  ( cd libmudflap
    cp -a \
      ChangeLog \
    $PKG1/usr/doc/gcc-${VERSION}/libmudflap
  )

  mkdir -p $PKG1/usr/doc/gcc-${VERSION}/libgomp
  ( cd libgomp
    cp -a \
      ChangeLog \
    $PKG1/usr/doc/gcc-${VERSION}/libgomp
  )
  
  mkdir -p $PKG6/usr/doc/gcc-${VERSION}/libobjc
  ( cd libobjc
    cp -a \
      ChangeLog README README.threads THREADS THREADS.MACH \
    $PKG6/usr/doc/gcc-${VERSION}/libobjc
  )
    
  mkdir -p $PKG2/usr/doc/gcc-${VERSION}/libstdc++-v3
  ( cd libstdc++-v3
    cp -a \
      ChangeLog README \
    $PKG2/usr/doc/gcc-${VERSION}/libstdc++-v3
    
    cp -a \
      docs/html/faq \
    $PKG2/usr/doc/gcc-${VERSION}/libstdc++-v3
  )
)

# Add fastjar to the gcc-java package:
( cd $TMP
  FASTJARVER=$(echo $CWD/fastjar-*.tar.xz | rev | cut -f 3- -d . | cut -f 1 -d - | rev)
  echo
  echo "Building fastjar-$FASTJARVER first"
  echo
  rm -rf fastjar-$FASTJARVER
  tar xvf $CWD/fastjar-$FASTJARVER.tar.xz || exit 1
  cd fastjar-$FASTJARVER || exit
  chown -R root:root .
  find . \
    \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
    -exec chmod 755 {} \; -o \
    \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
    -exec chmod 644 {} \;
  for patch in $CWD/fastjar-patches/*.gz ; do
    zcat $patch | patch -p1 --verbose || exit 1
  done
  CFLAGS="$SLKCFLAGS" \
  CXXFLAGS="$SLKCFLAGS" \
  ./configure \
     --prefix=/usr \
     --libdir=/usr/lib$LIBDIRSUFFIX \
     --mandir=/usr/man \
     --infodir=/usr/info \
     --build=$TARGET
  make $NUMJOBS || exit 1
  make install DESTDIR=$PKG5 || exit 1
  mkdir -p $PKG5/usr/doc/fastjar-$FASTJARVER
  cp -a \
    AUTHORS CHANGES COPYING* INSTALL NEWS README* TODO \
    $PKG5/usr/doc/fastjar-$FASTJARVER
  # If there's a ChangeLog, installing at least part of the recent history
  # is useful, but don't let it get totally out of control:
  if [ -r ChangeLog ]; then
    DOCSDIR=$(echo $PKG5/usr/doc/fastjar-$FASTJARVER)
    cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
    touch -r ChangeLog $DOCSDIR/ChangeLog
  fi
  find $PKG5 | xargs file | grep -e "executable" -e "shared object" | grep ELF \
    | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  # Compress and if needed symlink the man pages:
  if [ -d $PKG5/usr/man ]; then
    ( cd $PKG5/usr/man
      for manpagedir in $(find . -type d -name "man*") ; do
        ( cd $manpagedir
          for eachpage in $( find . -type l -maxdepth 1) ; do
            ln -s $( readlink $eachpage ).gz $eachpage.gz
            rm $eachpage
          done
          gzip -9 *.?
        )
      done
    )
  fi
  # Compress info files, if any:
  if [ -d $PKG5/usr/info ]; then
    ( cd $PKG5/usr/info
      rm -f dir
      gzip -9 *
    )
  fi
  echo
) || exit 1

# build gcc
( mkdir gcc.build.lnx;
  cd gcc.build.lnx;

  # If enough people need "treelang" support for it may be considered.
  #  --enable-languages=ada,c,c++,fortran,java,objc,treelang
  # I think it's incorrect to include this option (as it'll end up set
  # to i486 on x86 platforms), and we want to tune the binary structure
  # for i686, as that's where almost all of the optimization speedups
  # are to be found.
  # Correct me if my take on this is wrong.
  #  --with-cpu=$ARCH 
 
  if [ "$ARCH" != "x86_64" ]; then
    GCC_ARCHOPTS="--with-arch=$ARCH"
  else
    GCC_ARCHOPTS="--disable-multilib"
  fi
 
  CFLAGS="$SLKCFLAGS" \
  CXXFLAGS="$SLKCFLAGS" \
  ../gcc-$VERSION/configure --prefix=/usr \
     --libdir=/usr/lib$LIBDIRSUFFIX \
     --enable-shared --enable-libstdcxx-time \
     --enable-bootstrap \
     --enable-languages=c,c++,fortran,go,java \
     --enable-threads=posix \
     --enable-checking=release \
     --with-system-zlib \
     --disable-libunwind-exceptions \
     --enable-__cxa_atexit \
     --enable-libssp \
     --with-gnu-ld \
     --verbose \
     $GCC_ARCHOPTS \
     --target=${TARGET} \
     --build=${TARGET} \
     --host=${TARGET} || exit 1

  # Start the build:

  # Include all debugging info (for now):
  make $NUMJOB bootstrap;

  ( cd gcc
    make $NUMJOB gnatlib GNATLIBCFLAGS="$SLKCFLAGS"
    # This wants a shared -ladd2line?
    #make gnatlib-shared
    
    CFLAGS="$SLKCFLAGS" \
    CXXFLAGS="$SLKCFLAGS" \
    make $NUMJOB gnattools
  )
  make info

  # Set GCCCHECK=something to run the tests
  if [ ! -z $GCCCHECK ]; then
    make $NUMJOB check
  fi

  make install DESTDIR=$PKG1

#Be sure the "specs" file is installed.
if [ ! -r $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/${TARGET}/${VERSION}/specs ]; then
  cat stage1-gcc/specs > $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/${TARGET}/${VERSION}/specs
fi

# Make our 64bit gcc look for 32bit gcc binaries in ./32 subdirectory:
if [ "$ARCH" = "x86_64" ]; then
  sed -i 's#;.\(:../lib !m64 m32;\)$#;32\1#' \
    $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/${TARGET}/${VERSION}/specs
fi

#  make ada.install-common DESTDIR=$PKG1
#  make install-gnatlib DESTDIR=$PKG1
  make -i install-info DESTDIR=$PKG1

  chmod 755 $PKG1/usr/lib${LIBDIRSUFFIX}/libgcc_s.so.1

  # This is provided by binutils, so delete it here:
  rm -f $PKG1/usr/lib${LIBDIRSUFFIX}/libiberty.a
  rm -f $PKG1/usr/lib/libiberty.a # catch-all

  # Fix stuff up:
  ( cd $PKG1;mkdir -p usr/share/gdb/python/auto-load/;mv usr/lib${LIBDIRSUFFIX}/*.py \
    usr/share/gdb/python/auto-load/ )
  ( cd $PKG1/usr/;mv share/info . )
  ( cd $PKG1/usr/info ; rm dir ; gzip -9 * )
  ( cd $PKG1
    # *not* ${LIBDIRSUFFIX}
    mkdir -p lib
    cd lib
    ln -sf /usr/bin/cpp .
  )
  ( cd $PKG1/usr/bin
    mv g++ g++-gcc-$VERSION
    mv gcc gcc-$VERSION
    mv ${TARGET}-gfortran gfortran-gcc-$VERSION
    ln -sf g++-gcc-$VERSION g++
    ln -sf gcc-$VERSION gcc
    ln -sf g++ c++
    ln -sf gcc cc
    ln -sf gcc-$VERSION ${TARGET}-gcc
    ln -sf gcc-$VERSION ${TARGET}-gcc-$VERSION
    ln -sf gcj ${TARGET}-gcj
    ln -sf gcjh ${TARGET}-gcjh
    ln -sf g++-gcc-$VERSION ${TARGET}-c++
    ln -sf g++-gcc-$VERSION ${TARGET}-g++
    ln -sf gfortran-gcc-$VERSION gfortran
    ln -sf gfortran-gcc-$VERSION ${TARGET}-gfortran
    ln -sf gfortran-gcc-$VERSION ${TARGET}-gfortran-$VERSION
    ln -sf gfortran-gcc-$VERSION ${TARGET}-g95
    ln -sf gfortran g95
    ln -sf gfortran f95
    ln -sf gfortran-gcc-$VERSION ${TARGET}-g77
    ln -sf gfortran g77
    ln -sf gfortran f77
  )
  ( cd $PKG1/usr/;mv share/man/ . )
  ( cd $PKG1/usr/man
    gzip -9 */*
    cd man1
    ln -sf g++.1.gz c++.1.gz
    ln -sf gcc.1.gz cc.1.gz
  )  

  ## build the txz package
  #(
  #  cd $PKG1;
  #  makepkg -l y -c n $TMP/gcc-$VERSION-$ARCH-$BUILD.txz
  #)
# keep a log
) 2>&1 | tee $TMP/gcc.build.log

# OK, time to split the big package where needed:

# gcc-g++:
( cd $PKG2
  mkdir -p usr/share/gdb/python/auto-load/;mv usr/lib${LIBDIRSUFFIX}/*.py \
    usr/share/gdb/python/auto-load/
  mkdir -p usr/bin
  mv $PKG1/usr/bin/*++* usr/bin
  mkdir -p usr/include
  mv $PKG1/usr/include/c++ usr/include
  mkdir -p usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/*++* usr/lib${LIBDIRSUFFIX}
  mkdir -p usr/libexec/gcc/$TARGET/$VERSION
  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/cc1plus usr/libexec/gcc/$TARGET/$VERSION/cc1plus
  mkdir -p usr/man/man1
  mv $PKG1/usr/man/man1/*++* usr/man/man1
)

# gcc-gfortran:
( cd $PKG3
  mkdir -p usr/share/gdb/python/auto-load/;mv usr/lib${LIBDIRSUFFIX}/*.py \
    usr/share/gdb/python/auto-load/
  mkdir -p usr/bin
  mv $PKG1/usr/bin/*gfortran* usr/bin
  mv $PKG1/usr/bin/*95* usr/bin
  mv $PKG1/usr/bin/*77* usr/bin
  # Doesnt this seem like a logical idea?
  ( cd usr/bin ; ln -sf gfortran-gcc-${VERSION} fortran )
  mkdir -p usr/info
  mv $PKG1/usr/info/gfortran* usr/info
  mkdir -p usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/libgfortran* usr/lib${LIBDIRSUFFIX}
  mkdir -p usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/finclude usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/libgfortran* usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION
  mkdir -p usr/libexec/gcc/$TARGET/$VERSION
  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/f951 usr/libexec/gcc/$TARGET/$VERSION/f951
  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/libgfortran* usr/libexec/gcc/$TARGET/$VERSION
  mkdir -p usr/man/man1
  mv $PKG1/usr/man/man1/gfortran* usr/man/man1
)

# gcc-gnat:
#( cd $PKG4
#  mkdir -p usr/bin
#  mv $PKG1/usr/bin/gnat* usr/bin
#  mv $PKG1/usr/bin/gpr* usr/bin
#  mkdir -p usr/info
#  mv $PKG1/usr/info/gnat* usr/info
#  mkdir -p usr/libexec/gcc/$TARGET/$VERSION
#  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/gnat1 usr/libexec/gcc/$TARGET/$VERSION
#  mkdir -p usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION
#  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/adainclude usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION
#  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/adalib usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION
#)

# gcc-java:
( cd $PKG5
  mkdir -p usr/share/gdb/python/auto-load/;mv usr/lib${LIBDIRSUFFIX}/*.py \
    usr/share/gdb/python/auto-load/
  mkdir -p usr/share
  mv $PKG1/usr/share/java usr/share
  cp -v $CWD/ecj-4.5.jar usr/share/java/eclipse-ecj.jar
  mkdir -p usr/bin
  mv $PKG1/usr/bin/addr2name.awk usr/bin
  # mv $PKG1/usr/bin/fastjar usr/bin
  mv $PKG1/usr/bin/gappletviewer usr/bin
  mv $PKG1/usr/bin/gc-analyze usr/bin
  mv $PKG1/usr/bin/*gcj* usr/bin
  mv $PKG1/usr/bin/gij usr/bin
  mv $PKG1/usr/bin/gjar usr/bin
  mv $PKG1/usr/bin/gjarsigner usr/bin
  mv $PKG1/usr/bin/gkeytool usr/bin
  mv $PKG1/usr/bin/grepjar usr/bin
  mv $PKG1/usr/bin/grmid usr/bin
  mv $PKG1/usr/bin/grmic usr/bin
  mv $PKG1/usr/bin/grmiregistry usr/bin
  mv $PKG1/usr/bin/gserialver usr/bin
  mv $PKG1/usr/bin/gtnameserv usr/bin
  mv $PKG1/usr/bin/gjavah usr/bin
  mv $PKG1/usr/bin/gorbd usr/bin
  mv $PKG1/usr/bin/jar usr/bin
  mv $PKG1/usr/bin/jcf-dump usr/bin
  mv $PKG1/usr/bin/jv-* usr/bin
  mv $PKG1/usr/bin/rmi* usr/bin
  mkdir -p usr/include
  mv $PKG1/usr/include/ffi.h usr/include
  mkdir -p usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/jawt.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/ffi.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/ffitarget.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/jawt_md.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/jni.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/jni_md.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/jvmpi.h usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/libffi usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mkdir -p usr/info
  mv $PKG1/usr/info/gcj.* usr/info
  # mv $PKG1/usr/info/fastjar.* usr/info
  mkdir -p usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/libffi* usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/logging.properties usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcj* usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/libgcj* usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/libgij* usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/lib-org* usr/lib${LIBDIRSUFFIX}
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/security usr/lib${LIBDIRSUFFIX}
  mkdir -p usr/lib${LIBDIRSUFFIX}/pkgconfig
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/pkgconfig/libgcj*.pc usr/lib${LIBDIRSUFFIX}/pkgconfig
  rmdir $PKG1/usr/lib${LIBDIRSUFFIX}/pkgconfig 2> /dev/null
  mkdir -p usr/libexec/gcc/$TARGET/$VERSION
  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/jc1 usr/libexec/gcc/$TARGET/$VERSION
  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/ecj1 usr/libexec/gcc/$TARGET/$VERSION
  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/jvgenmain usr/libexec/gcc/$TARGET/$VERSION
  mkdir -p usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/gcj usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
  mkdir -p usr/man/man1
  mv $PKG1/usr/man/man1/gappletviewer.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gc-analyze.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gcj.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gcjh.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gcj-dbtool.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gij.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gjar.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gjarsigner.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gjavah.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gjnih.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gkeytool.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gnative2ascii.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gorbd.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/grmic.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/grmid.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/grmiregistry.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gserialver.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/gtnameserv.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/jcf-dump.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/jv-convert.1.gz usr/man/man1
  mv $PKG1/usr/man/man1/jv-scan.1.gz usr/man/man1
  # The ecj wrapper script:
  cat $CWD/ecj.sh | sed -e "s,@JAVADIR@,/usr/share/java," > usr/bin/ecj
  cat $CWD/ecj1 | sed -e "s,@JAVADIR@,/usr/share/java," > usr/bin/ecj1
  chmod 755 usr/bin/ecj
  chmod 755 usr/bin/ecj1
)

# gcc-objc:
#( cd $PKG6
#  mkdir -p usr/lib${LIBDIRSUFFIX}
#  mv $PKG1/usr/lib${LIBDIRSUFFIX}/libobjc* usr/lib${LIBDIRSUFFIX}
#  mkdir -p usr/libexec/gcc/$TARGET/$VERSION
#  mv $PKG1/usr/libexec/gcc/$TARGET/$VERSION/cc1obj usr/libexec/gcc/$TARGET/$VERSION
#  mkdir -p usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
#  mv $PKG1/usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include/objc usr/lib${LIBDIRSUFFIX}/gcc/$TARGET/$VERSION/include
#)

## NOTE: Thought about this, because the precompiled headers are so large.
## Probably easier to keep everything together, though.
## gcc-g++-gch (precompiled c++ headers)
#( cd $PKG7
#  mkdir -p usr/include/c++/$VERSION/$TARGET/bits
#  mv $PKG2/usr/include/c++/$VERSION/$TARGET/bits/stdc++.h.gch usr/include/c++/$VERSION/$TARGET/bits
#)

# Filter all .la files (thanks much to Mark Post for the sed script):
( cd $TMP
  for file in $(find . -type f -name "*.la") ; do
    cat $file | sed -e 's%-L/gcc-[[:graph:]]* % %g' > $TMP/tmp-la-file
    cat $TMP/tmp-la-file > $file
  done
  rm $TMP/tmp-la-file
)

## Strip bloated binaries and libraries:
for dir in $PKG{1,2,3,4,5,6}; do
  ( cd $dir
    find . -name "lib*so*" -exec strip --strip-unneeded "{}" \;
    find . -name "lib*a" -exec strip -g "{}" \;
    strip --strip-unneeded usr/bin/* 2> /dev/null
    find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
    find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  )
done

( cd $PKG1
  makepkg -l y -c n $PKGDIR/gcc-$VERSION-$ARCH-$BUILD.txz )
( cd $PKG2
  makepkg -l y -c n $PKGDIR/gcc-g++-$VERSION-$ARCH-$BUILD.txz )
( cd $PKG3
  makepkg -l y -c n $PKGDIR/gcc-gfortran-$VERSION-$ARCH-$BUILD.txz )
#( cd $PKG4
#  makepkg -l y -c n $PKGDIR/gcc-gnat-$VERSION-$ARCH-$BUILD.txz )
( cd $PKG5
  makepkg -l y -c n $PKGDIR/gcc-java-$VERSION-$ARCH-$BUILD.txz )
#( cd $PKG6
#  makepkg -l y -c n $PKGDIR/gcc-objc-$VERSION-$ARCH-$BUILD.txz )
#( cd $PKG7
#  makepkg -l y -c n $PKGDIR/gcc-g++-gch-$VERSION-$ARCH-$BUILD.txz )
#( cd $PKG8
#  makepkg -l y -c n $PKGDIR/gcc-go-$VERSION-$ARCH-$BUILD.txz )

echo
echo "Slackware GCC package build complete!"
echo

