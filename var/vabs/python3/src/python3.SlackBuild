#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .txz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
#
# This Template was compiled from the contributions of many users of the Vector
# Linux forum at http://forum.vectorlinux.com and from tidbits collected 
# from all over the internet. 
#
# Generated by sbbuilder-0.4.14.1, written by Rodrigo Bistolfi 
# (rbistolfi) and Raimon Grau Cuscó (Kidd) for VectorLinux.
#
# Please put your name below if you add some original scripting lines.
# AUTHORS = 

NAME="python3"            #Enter package Name!
VERSION=${VERSION:-"3.5.0"}      #Enter package Version!
PYVER=${VERSION%.*}
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"hata_ph"}   #Enter your Name!
LINK=${LINK:-"http://python.org/ftp/python/$VERSION/Python-$VERSION.tar.xz \
http://www.python.org/ftp/python/doc/$VERSION/python-$VERSION-docs-html.tar.bz2"}  #Enter URL for package here!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"slapt-get openssl libpng freetype tk tcl sqlite db44"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------

# Location for Python site-packages:
SITEPK=$PKG/usr/lib${LIBDIRSUFFIX}/python${PYVER}/site-packages
# same as above without $PKG
TOOLSDIR=/usr/lib${LIBDIRSUFFIX}/python${PYVER}/site-packages
# Enable external python docs. Default is Yes.
DOCS="YES"

if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------


#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------


rm -rf $PKG
cd $TMP
rm -rf $NAME-$VERSION


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xvf $CWD/Python-$VERSION.tar.* || exit 1
#this moves whatever was extracted to the std dirname we are expecting
mv * $NAME-$VERSION &> /dev/null 2>&1
mkdir -p $PKG
#-----------------------------------------------------


cd $TMP/$NAME-$VERSION


#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
#for i in $CWD/patches/*;do
#  patch -p1 <$i
#  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
#  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
#done
#-----------------------------------------------------
 
patch -p1 < $CWD/patches/python3.readline.set_pre_input_hook.diff || { echo "Failed to apply patch: python3.readline.set_pre_input_hook.diff"; exit 1; }
# We don't want a large libpython*.a:
#patch -p0 < $CWD/patches/python3.no-static-library.diff || { echo "Failed to apply python3.no-static-library.diff"; exit 1; }

# Fix python3 path in cgi.py.
sed -i '1s|^#.*/usr/local/bin/python|#!/usr/bin/python3|' Lib/cgi.py 

#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

echo "Configuring source..."
CXX="/usr/bin/g++" \
	./configure --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --with-threads \
  --enable-ipv6 \
  --enable-shared \
  --with-system-expat \
  --with-system-ffi \
  --program-prefix="" \
  --program-suffix="" \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make || exit 1

make install DESTDIR=$PKG || exit 1


#######################################################################
#Miscellenious tweaks and things outside a normal ./configure go here #
#######################################################################

( cd $PKG/usr/bin
  ln -sf python${PYVER} python3
  ln -sf python${PYVER}*-config python${PYVER}-config
  ln -sf pydoc${PYVER} pydoc3
  ln -sf pyvenv-${PYVER} pyvenv
  ln -sf idle${PYVER} idle3
)

# We'll install the python-tools under site-packages:
mkdir -p $SITEPK
cp -a Tools/* $SITEPK

if [ "$DOCS" = "YES" ]; then
	tar xf $CWD/python-$VERSION-docs-html.tar.bz2
	mkdir -p $PKG/usr/doc/$NAME-$VERSION
	mv python-$VERSION-docs-html $PKG/usr/doc/python3-$VERSION/html
fi

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a README LICENSE Misc $PKG/usr/doc/$NAME-$VERSION
mv $SITEPK/README $PKG/usr/doc/$NAME-$VERSION/README.python-tools
( cd $PKG/usr/doc/$NAME-$VERSION ; ln -sf $TOOLSDIR Tools )
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild
chown -R root:root $PKG/usr/doc/$NAME-$VERSION
	
#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ];then
mkdir -p $PKG/usr/man
mv $PKG/usr/share/man/* $PKG/usr/man
rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;

if [ -d $PKG/usr/share/info ];then
mkdir -p $PKG/usr/info
mv $PKG/usr/share/info/* $PKG/usr/info
rm -rf $PKG/usr/share/info
fi 
find $PKG/usr/info -type f -exec gzip -9 {} \;

mkdir -p $PKG/install
if [ -d $PKG/usr/info ];then
cat >> $PKG/install/doinst.sh << EOF
CWD=\$(pwd)
cd usr/info
if [ -f dir ];then
    rm dir
fi
if [ -f dir.gz ];then
    rm dir.gz
fi
for i in *.info.gz;do
    install-info \$i dir
done
cd \$CWD
EOF
fi

cat >> $PKG/install/doinst.sh << EOF
# update rarian database
if [ -x usr/bin/rarian-sk-update ]; then
  usr/bin/rarian-sk-update 1> /dev/null 2> /dev/null
fi

# update mime database
if [ -x usr/bin/update-mime-database ]; then
  usr/bin/update-mime-database usr/share/mime 1> /dev/null 2> /dev/null
fi

# update desktop entries
if [ -x usr/bin/update-desktop-database ]; then
  usr/bin/update-desktop-database 1> /dev/null 2> /dev/null
fi

# update hicolor icons
if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
	rm -f usr/share/icons/hicolor/icon-theme.cache
fi
usr/bin/gtk-update-icon-cache -f -q usr/share/icons/hicolor 1>/dev/null 2>/dev/null

if [ -x /usr/bin/glib-compile-schemas ]; then
  /usr/bin/glib-compile-schemas usr/share/glib-2.0/schemas/ >/dev/null 2>&1
fi

# Restart gconfd-2 if running to reload new gconf settings
if ps acx | grep -q gconfd-2 ; then
        killall -HUP gconfd-2 ;
fi

# A good idea whenever kernel modules are added or changed:
if [ -x sbin/depmod ]; then
  /sbin/depmod -a 1> /dev/null 2> /dev/null
fi
EOF

#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "$NAME")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 $NAME: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
$NAME: $NAME (object-oriented interpreted programming language)
$NAME:
$NAME: Python is an interpreted, interactive, object-oriented programming
$NAME: language that combines remarkable power with very clear syntax.
$NAME: This is a new version of the language that is incompatible with the
$NAME: 2.x line of releases.  The language is mostly the same, but many
$NAME: details, especially how built-in objects like dictionaries and strings
$NAME: work, have changed considerably, and a lot of deprecated features have
$NAME: finally been removed.  Also, the standard library has been reorganized
$NAME: in a few prominent places.
$NAME:
$NAME: License: Refer to LICENSE
$NAME: Authors: http://python.org/
$NAME: Website: http://python.org/

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\\$\ \.\/configure\ /" $TMP/$DIRNAME/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
find . -name *.la|xargs rm
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
fi
#--------------------------------------------------------------

# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
