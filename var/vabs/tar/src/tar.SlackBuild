#!/bin/sh

# Copyright 2005-2011  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Slackware build script for tar
NAME="tar"
OLDVER="1.13"
VERSION=${VERSION:-1.26}
LINK=${LINK:-"http://slackware.osuosl.org/slackware-current/source/a/$NAME/$NAME-$VERSION.tar.xz"}
LINK1=${LINK1:-"http://slackware.osuosl.org/slackware-current/source/a/$NAME/$NAME-$OLDVER.tar.gz"}
NUMJOBS=${NUMJOBS:--j7}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-tar

rm -rf $PKG
mkdir -p $TMP $PKG

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

#get the source..........
for SRC in $(echo $LINK $LINK1);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
# This old version is the only one that won't clobber symlinks, e.g.:
# someone moves /opt to /usr/opt and makes a symlink.  With newer
# versions of tar, installing any new package will remove the /opt
# symlink and plop down a new directory there.
# Well, there's a lot of other bugs (the remote stuff particularly I'm
# told is flaky) in tar-1.13, so it'll only be here now for use by the
# Slackware package utils.  And, we'll even let people remove it and
# the pkgutils will still try to work (but eventually they'll pay the
# price :)
cd $TMP
rm -rf tar-1.13
tar xvf $CWD/tar-1.13.tar.gz || exit 1
cd tar-1.13 || exit 1
zcat $CWD/tar-1.13.bzip2.diff.gz | patch -p1 --verbose || exit 1
# The original config.{guess,sub} do not work on x86_64
cp -p /usr/share/libtool/config/config.{guess,sub} .
chown -R root:root .
#fix build with glibc-2.16
sed -i -e '/gets is a/d' gnu/stdio.in.h

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --disable-nls \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

  make $NUMJOBS || make || exit 1
mkdir -p $PKG/bin
cat src/tar > $PKG/bin/tar-1.13
chmod 0755 $PKG/bin/tar-1.13
# End building of tar-1.13

cd $TMP
rm -rf tar-$VERSION
tar xvf $CWD/tar-$VERSION.tar.xz || exit 1
cd tar-$VERSION || exit 1
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Don't spew "Record size = foo blocks" messages:
zcat $CWD/tar.norecordsizespam.diff.gz | patch -p1 --verbose || exit 1

# The "A lone zero block at %s" messages also cause problems:
zcat $CWD/tar.nolonezero.diff.gz | patch -p1 --verbose || exit 1

# Add support for *.txz files (our packages)
zcat $CWD/tar-1.2x.support_txz.diff.gz | patch -p1 --verbose || exit 1

#fix build with glibc-2.16
sed -i -e '/gets is a/d' gnu/stdio.in.h

FORCE_UNSAFE_CONFIGURE=1 \
CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --infodir=/usr/info \
  --docdir=/usr/doc/tar-$VERSION \
  --enable-backup-scripts \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | \
    xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | \
    xargs strip --strip-unneeded 2> /dev/null
)

mv $PKG/usr/bin/tar $PKG/bin
( cd $PKG/usr/bin ; ln -sf /bin/tar . )
( cd $PKG/bin ; ln -sf tar tar-$VERSION )

# Support "historic" rmt locations:
mkdir -p $PKG/{etc,sbin}
( cd $PKG/etc
  ln -sf /usr/libexec/rmt .
  cd $PKG/sbin
  ln -sf /usr/libexec/rmt .
)

mkdir -p $PKG/usr/man/man{1,8}
cat $CWD/tar.1.gz > $PKG/usr/man/man1/tar.1.gz
cat $CWD/rmt.8.gz > $PKG/usr/man/man8/rmt.8.gz

rm -f $PKG/usr/info/dir
gzip -9 $PKG/usr/info/*

mkdir -p $PKG/usr/doc/tar-$VERSION
cp -a \
  ABOUT-NLS AUTHORS COPYING NEWS PORTS README THANKS TODO \
  $PKG/usr/doc/tar-$VERSION

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP

# Clean up the extra stuff:
if [ "$1" = "--cleanup" ]; then
  rm -rf $TMP/tar-$VERSION
  rm -rf $PKG
fi
fi
