#!/bin/sh

# Slackware build script for pasang-emas

# Written by Larry Hajali <larryhaja[at]gmail[dot]com>

NAME=pasang-emas
VERSION=3.1.0
BUILD=${BUILD:-2}
TAG=${TAG:-_SBo}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$NAME
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $NAME-$VERSION
tar xvf $CWD/$NAME-$VERSION.tar.bz2
cd $NAME-$VERSION
tar xvf $CWD/$NAME-themes-1.0.tar.bz2
tar xvf $CWD/pet-marble.tar.bz2
tar xvf $CWD/pet-fragrance.tar.bz2
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
LIBS="-lm" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --with-help-dir=/usr/share/gnome/help \
  --disable-scrollkeeper \
  --build=$ARCH-slackware-linux

make
make install DESTDIR=$PKG

# Install the themes.
cd $NAME-themes-1.0
  ./configure \
    --prefix=/usr
  make install DESTDIR=$PKG
cd ..

cp -ar marble $PKG/usr/share/$NAME/themes
cp -ar fragrance $PKG/usr/share/$NAME/themes

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a \
  AUTHORS ChangeLog COPYING INSTALL NEWS README \
  $PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$NAME-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
