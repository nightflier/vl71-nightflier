#!/bin/bash

# Test to make sure the pkg produced a binary
printf "Test for program binary ... "
test -f /usr/bin/htop || exit 1
printf "PASS \n"
